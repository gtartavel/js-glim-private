/*jslint vars: true, browser: true */
/*global glim, HTMLVideoElement */


// True when a video is playing
var PLAYING = false;

// Some GLIM functions
var GLIM_FUNCTIONS = {

    // Gradient in a given direction
    'grad': new glim.Function([
        'uniform vec2 direction;',
        'vec3 function (vec3 color) {',
        '   vec3 next = texture2D(images[0], position + px * direction).rgb;',
        '   vec3 prev = texture2D(images[0], position - px * direction).rgb;',
        '   return next - prev;',
        '}',
        ''].join('\n')),

    // Pixel-wise norm (e.g. on gradients)
    'norm': new glim.Function([
        'vec3 function (vec3 fst, vec3 snd) {',
        '   return sqrt(fst*fst + snd*snd);',
        '}',
        ''].join('\n'))
};

// Display a hint in the console
function hint() {
    'use strict';
    console.log('Hint: redefine the global RUN function to execute your own glim.Function!');
}

// Run the function(s) on the input
function RUN(input) {
    'use strict';
    var xgrad = GLIM_FUNCTIONS.grad.run([input], false, {'direction': [1, 0]});
    var ygrad = GLIM_FUNCTIONS.grad.run([input], false, {'direction': [0, 1]});
    GLIM_FUNCTIONS.norm.run([xgrad, ygrad]);

    // Delete temporary GPU images
    xgrad.clear();
    ygrad.clear();
}

// Run the GLIM functions on an image or a video, and loop if needed
function runStep(input) {
    'use strict';
    if (input.style.display.toLowerCase() === 'none') {
        return;
    }

    // Run the program(s)
    RUN(input);

    // If video, display next frame after a while
    PLAYING = (input instanceof HTMLVideoElement) && !input.paused && !input.ended;
    if (PLAYING) {
        glim.requestAnimationFrame(function () {
            runStep(input);
        });
    }
}


// Initialize the HTML elements
function init() {
    'use strict';
    hint();

    // Shortcut to access HTML elements
    var $ = function (id) {
        return window.document.getElementById(id);
    };

    // Append the rendering canvas to the page
    var canvas = glim.getCanvas();
    $('out_div').appendChild(canvas);
    canvas.width = 48;
    canvas.height = 48;

    // Define callbacks for file loading
    var fileLoader = glim.getFileLoader(false, {
        'image/*': function (data) {
            var image = $('in_image');
            image.onload = function () {
                image.style.display = '';
                runStep(image);
            };
            $('in_video').pause();
            $('in_video').style.display = 'none';
            image.src = data;
        },
        'video/*': function (data) {
            var video = $('in_video');
            video.onloadedmetadata = function () {
                video.width = video.videoWidth;
                video.height = video.videoHeight;
                video.style.display = '';
            };
            video.onplay = function () {
                if (!PLAYING) {
                    runStep(video);
                }
            };
            $('in_image').style.display = 'none';
            video.src = data;
        }
    });

    // Use this callback for click and drag'n'drop
    $('in_drop').ondragover = fileLoader;
    $('in_drop').ondrop = fileLoader;
    $('in_drop').onclick = fileLoader;
}