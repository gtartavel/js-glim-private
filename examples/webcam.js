/*jslint vars: true, browser: true */
/*global glim, HTMLVideoElement */


// Some GLIM functions
var GLIM_FUNCTIONS = {

    // Gradient in a given direction
    'grad': new glim.Function([
        'uniform vec2 direction;',
        'vec3 function (vec3 color) {',
        '   vec3 next = texture2D(images[0], position + px * direction).rgb;',
        '   vec3 prev = texture2D(images[0], position - px * direction).rgb;',
        '   return next - prev;',
        '}',
        ''].join('\n')),

    // Pixel-wise norm (e.g. on gradients)
    'norm': new glim.Function([
        'vec3 function (vec3 fst, vec3 snd) {',
        '   return sqrt(fst*fst + snd*snd);',
        '}',
        ''].join('\n'))
};

// Display a hint in the console
function hint() {
    'use strict';
    console.log('Hint: you can override the RUN function!');
}

// Run the function(s) on the input
function RUN(input) {
    'use strict';
    var xgrad = GLIM_FUNCTIONS.grad.run([input], false, {'direction': [1, 0]});
    var ygrad = GLIM_FUNCTIONS.grad.run([input], false, {'direction': [0, 1]});
    GLIM_FUNCTIONS.norm.run([xgrad, ygrad]);

    // Delete temporary GPU images
    xgrad.clear();
    ygrad.clear();
}

// Run the GLIM functions on an image or a video, and loop if needed
function runStep(input) {
    'use strict';
    if (!input.paused && !input.ended) {
        var t = new Date().getTime();
        RUN(input);
        var dt = new Date().getTime() - t;
        var fps_str = Math.round(1000 / dt) + ' fps, ' + dt + ' ms / frame';
        document.getElementById('out_fps').value = fps_str;
    }
    glim.requestAnimationFrame(function () {
        runStep(input);
    });
}


// Initialize the HTML elements
function init() {
    'use strict';
    hint();

    // Shortcut to access HTML elements
    var $ = function (id) {
        return window.document.getElementById(id);
    };

    // Append the rendering canvas to the page
    var canvas = glim.getCanvas();
    $('out_div').appendChild(canvas);
    canvas.width = 48;
    canvas.height = 48;

    // Configure video callbacks
    var video = $('in_video');
    var playing = false;
    video.oncanplay = function () {
        video.width = video.videoWidth;
        video.height = video.videoHeight;
        $('help').style.display = 'none';
        $('out_div').style.display = '';
        console.log('Starting webcam at resolution ' + video.width + 'x' + video.height);
        if (!playing) {
            playing = true;
            runStep(video);
        }
    };

    // Start the camera
    var constraints = {
        'optional': [{'minWidth': 1280}, {'minHeight': 720}, {'minFrameRate': 60}, {'minFrameRate': 30}]
    };
    var getMedia = (navigator.getUserMedia ||
                    navigator.webkitGetUserMedia ||
                    navigator.mozGetUserMedia ||
                    navigator.msGetUserMedia).bind(navigator);
    var errorCallback = function (error) { window.alert('Cannot start webcam: ' + error); };
    var callback = function (stream) {
        if (navigator.mozGetUserMedia) {
            video.mozSrcObject = stream;
        } else {
            video.src = (window.URL || window.webkitURL).createObjectURL(stream);
        }
        video.play();
    };
    getMedia({'video': constraints, 'audio': false}, callback, errorCallback);
}