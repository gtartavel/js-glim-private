/* These classes are declared to avoid jsduck warning */

/** @private @class File */
/** @private @class HTMLCanvasElement */

/** @private @class Int8Array */
/** @private @class Uint8Array */
/** @private @class Uint8ClampedArray */
/** @private @class Int166Array */
/** @private @class Uint166Array */
/** @private @class Int32Array */
/** @private @class Uint32Array */
/** @private @class Float32Array */
/** @private @class Float64Array */

/** @private @class WebGLRenderingContext */
/** @private @class WebGLFramebuffer */
/** @private @class WebGLTexture */
/** @private @class WebGLShader */
/** @private @class WebGLProgram */
