// Configuration of Buster.js
module.exports["GLIM Unit Tests"] = {
    environment: "browser",
    rootPath: "../",
    libs: [
        'lib/check-types.js'
    ],
    src: [
        'src/glim.object.js',
        'src/glim.Image.class.js',
        'src/[^_]*.js'  // other files (order doesn't matter)
    ],
    tests: [
        "test/*.test.js"
    ],
    extensions: [
        // require('buster-istanbul')
    ]
};
