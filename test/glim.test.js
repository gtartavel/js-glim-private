/*jslint nomen: true, vars: true, browser: true */
/*global buster, glim, check, File, Uint8Array, WebGLRenderingContext */

'use strict';

buster.testCase('glim object', {
    'global variables': {
        '"glim" is an object': function () {
            buster.assert.isObject(glim);
        },
        '"check" is an object': function () {
            buster.assert.isObject(check);
        }
    },

    '.isAvailable()': {
        'returns true': function () {
            buster.assert.isTrue(glim.isAvailable());
        }
    },

    '.getCanvas()': {
        'returns a canvas': function () {
            buster.assert.tagName(glim.getCanvas(), 'canvas');
        },
        'always returns the same canvas': function () {
            var canvas = glim.getCanvas();
            buster.assert.same(glim.getCanvas(), canvas);
        }
    },

    '.getContext()': {
        'returns a rendering context': function () {
            buster.assert.hasPrototype(glim.getContext(), WebGLRenderingContext.prototype);
        },
        'returns getCanvas()\'s context': function () {
            buster.assert.same(glim.getContext().canvas, glim.getCanvas());
        },
        'throws if non-boolean argument': function () {
            buster.assert.exception(function () {
                glim.getContext(42);
            });
        }
    },

    '.requestAnimationFrame()': function (done) {
        glim.requestAnimationFrame(done(function () {
            return;
        }));
    },

    'handles no-support': {
        'setUp': function () {
            this.former_glim_context = glim._context;
        },
        'tearDown': function () {
            glim._context = this.former_glim_context;
        },
        'WebGL not supported': {
            'setUp': function () {
                this.stub(glim.getCanvas(), 'getContext', function () {
                    throw new Error('stub: canvas.getContext() throws');
                });
                delete glim._context;
            },
            '.isAvailable() returns false': function () {
                buster.assert.isFalse(glim.isAvailable());
            },
            '.getContext() throws': function () {
                buster.assert.exception(function () {
                    glim.getContext();
                });
            },
            '.getContext(true) returns null': function () {
                buster.assert.isNull(glim.getContext(true));
            }
        },
        'texture-float extension not supported': {
            'setUp': function () {
                this.stub(glim.getContext(), 'getExtension', function () {
                    return null;
                });
                delete glim._context;
            },
            '.isAvailable() returns false': function () {
                buster.assert.isFalse(glim.isAvailable());
            },
            '.getContext() throws': function () {
                buster.assert.exception(function () {
                    glim.getContext();
                });
            },
            '.getContext(true) returns null': function () {
                buster.assert.isNull(glim.getContext(true));
            }
        }
    },

    '.readFile()': {
        'reads the file content': {
            'setUp': function () {
                this.datastr = 'This is...\nSP4RT@!';
                this.dataurl = 'data:text/plain;base64,VGhpcyBpcy4uLgpTUDRSVEAh';
                this.file = new File([this.datastr], 'myfile.txt', {'type': 'text/plain'});
            },
            'read as text': function (done) {
                var file = this.file;
                var datastr = this.datastr;
                glim.readFile(this.file, 'text', done(function (dataRead) {
                    buster.assert.same(this, file);
                    buster.assert.equals(dataRead, datastr);
                }));
            },
            'read as dataurl': function (done) {
                var file = this.file;
                var dataurl = this.dataurl;
                glim.readFile(this.file, 'dataurl', done(function (dataRead) {
                    buster.assert.same(this, file);
                    buster.assert.equals(dataRead, dataurl);
                }));
            },
            'read as arraybuffer': function (done) {
                var file = this.file;
                var datastr = this.datastr;
                glim.readFile(this.file, 'arraybuffer', done(function (dataRead) {
                    var dataStr = String.fromCharCode.apply(null, new Uint8Array(dataRead));
                    buster.assert.same(this, file);
                    buster.assert.equals(dataStr, datastr);
                }));
            },
        },
        'raises if invalid arguments': function () {
            var file = new File([], 'empty');
            var callback = function () { return; };
            buster.refute.exception(function () { glim.readFile(file, 'text', callback); });
            buster.refute.exception(function () { glim.readFile(file, 'text', callback, callback); });
            buster.assert.exception(function () { glim.readFile(null, 'text', callback); });
            buster.assert.exception(function () { glim.readFile(file, null, callback); });
            buster.assert.exception(function () { glim.readFile(file, 'fooo', callback); });
            buster.assert.exception(function () { glim.readFile(file, 'text', null); });
            buster.assert.exception(function () { glim.readFile(file, 'text', callback, false); });
        },
        'invokes error callback if error': function (done) {
            var file = new File([], 'empty');
            this.stub(window, 'FileReader', function () {
                this.readAsText = function () {
                    this.onerror();
                };
            });
            glim.readFile(file, 'text', function () { return; }, done(function (e) {
                buster.assert.same(this, file);
                buster.assert.hasPrototype(e, Error.prototype);
            }));
        },
        'invokes error callback if abort': function (done) {
            var file = new File([], 'empty');
            this.stub(window, 'FileReader', function () {
                this.readAsText = function () {
                    this.onabort();
                };
            });
            glim.readFile(file, 'text', function () { return; }, done(function (e) {
                buster.assert.same(this, file);
                buster.assert.hasPrototype(e, Error.prototype);
            }));
        }
    },

    '.getFileLoader()': {
        'setUp': function () {
            this.readFileStub = this.stub(glim, 'readFile');
        },
        'calls the right callback': {
            'setUp': function () {
                this.callbacks = {
                    '*': function () { return; },
                    'image/*': function () { return; },
                    'image/png': function () { return; },
                    'text/plain': function () { return; }
                };
                this.loader = glim.getFileLoader(false, this.callbacks);
            },
            'simple case': function () {
                var file = new File([], '', {'type': 'text/plain'});
                this.loader(file);
                buster.assert.calledOnceWith(this.readFileStub, file, 'dataurl', this.callbacks['text/plain']);
            },
            'specific case': function () {
                var file = new File([], '', {'type': 'image/png'});
                this.loader(file);
                buster.assert.calledOnceWith(this.readFileStub, file, 'dataurl', this.callbacks['image/png']);
            },
            'category case': function () {
                var file = new File([], '', {'type': 'image/jpeg'});
                this.loader(file);
                buster.assert.calledOnceWith(this.readFileStub, file, 'dataurl', this.callbacks['image/*']);
            },
            'default case': function () {
                var file = new File([], '', {'type': 'video/mpeg'});
                this.loader(file);
                buster.assert.calledOnceWith(this.readFileStub, file, 'dataurl', this.callbacks['*']);
            }
        },
        'handles formats': {
            'setUp': function () {
                this.callbacks = {
                    'audio/mpeg:arraybuffer': function () { return; },
                    'image/*:   dataurl': function () { return; },
                    'text/* :   text': function () { return; }
                };
                this.loader = glim.getFileLoader(false, this.callbacks);
            },
            'arraybuffer': function () {
                var file = new File([], '', {'type': 'audio/mpeg'});
                this.loader(file);
                buster.assert.calledOnceWith(this.readFileStub, file, 'arraybuffer');
            },
            'dataurl': function () {
                var file = new File([], '', {'type': 'image/png'});
                this.loader(file);
                buster.assert.calledOnceWith(this.readFileStub, file, 'dataurl');
            },
            'text': function () {
                var file = new File([], '', {'type': 'text/plain'});
                this.loader(file);
                buster.assert.calledOnceWith(this.readFileStub, file, 'text');
            }
        },
        'handles file types': {
            'setUp': function () {
                this.image_callback = function () { return; };
                this.onerror = this.stub();
                this.callbacks = {
                    'my/loader': function () { return; },
                    'text/plain': 'my/loader',
                    'image/png,image/jpeg  ,  image/gif': this.image_callback
                };
                this.loader = glim.getFileLoader(false, this.callbacks, this.onerror);
            },
            'handle indirection': function () {
                var file = new File([], '', {'type': 'text/plain'});
                this.loader(file);
                buster.assert.calledOnceWith(this.readFileStub, file, 'dataurl', this.callbacks['my/loader']);
            },
            'handle multiple types at once': function () {
                var file_png = new File([], '', {'type': 'image/png'});
                var file_jpg = new File([], '', {'type': 'image/jpeg'});
                var file_gif = new File([], '', {'type': 'image/gif'});
                this.loader(file_png);
                this.loader(file_jpg);
                this.loader(file_gif);
                buster.assert.calledThrice(this.readFileStub);
                buster.assert.calledWith(this.readFileStub, file_png, 'dataurl', this.image_callback);
                buster.assert.calledWith(this.readFileStub, file_jpg, 'dataurl', this.image_callback);
                buster.assert.calledWith(this.readFileStub, file_gif, 'dataurl', this.image_callback);
            },
            'invokes error callback if invalid type': function () {
                var file = new File([], '', {'type': 'image/tiff'});
                this.loader(file);
                buster.refute.called(this.readFileStub);
                buster.assert.calledOn(this.onerror, file);
            }
        },
        'handles multiple files': {
            'setUp': function () {
                this.onerror = this.stub();
                this.callbacks = {'*': function () { return; }};
                this.fileList = [
                    new File([], '', {'type': 'text/plain'}),
                    new File([], '', {'type': 'text/plain'}),
                    new File([], '', {'type': 'text/plain'})
                ];
                this.loadSeveral = glim.getFileLoader(true, this.callbacks, this.onerror);
                this.loadSingle = glim.getFileLoader(false, this.callbacks, this.onerror);
            },
            'when enabled, handle multiple files': function () {
                this.loadSeveral(this.fileList);
                buster.assert.calledThrice(this.readFileStub);
                buster.assert.calledWith(this.readFileStub, this.fileList[0]);
                buster.assert.calledWith(this.readFileStub, this.fileList[1]);
                buster.assert.calledWith(this.readFileStub, this.fileList[2]);
            },
            'when disabled': {
                'handles single file': function () {
                    this.loadSingle(this.fileList[0]);
                    buster.assert.calledOnceWith(this.readFileStub, this.fileList[0]);
                },
                'handles array with single file': function () {
                    this.loadSingle([this.fileList[0]]);
                    buster.assert.calledOnceWith(this.readFileStub, this.fileList[0]);
                },
                'refuses array with severa files': function () {
                    this.loadSingle(this.fileList);
                    buster.refute.called(this.readFileStub);
                    buster.assert.calledOn(this.onerror, this.fileList);
                }
            }
        },
        '//handles events': function () {
            buster.assert(0);
            /* To be check:
             *  - dragover -> preventdefault
             *  - drop -> preventdefault & load files
             *  - click -> prompt for file
             */
        }
    }
});
