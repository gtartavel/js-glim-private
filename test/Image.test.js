/*jslint nomen: true, vars: true */
/*global buster, glim, window, Image,
         Int8Array, Uint8Array, Float32Array, Float64Array */

'use strict';

var array_almost_equals = function (array, ref, delta) {
    delta = delta || 1e-6;  // for float32 around 0..1

    // Check array shape
    if (!array || !ref) {
        return false;
    }
    if (array.constructor !== ref.constructor || array.length !== ref.length) {
        return false;
    }

    // Compute max error
    var diff, maxi = 0;
    var k, n = ref.length;
    for (k = 0; k < n; k += 1) {
        diff = Math.abs(array[k] - ref[k]);
        if (diff > maxi) {
            maxi = diff;
        }
    }
    return (maxi <= delta);
};

var testData = (function () {
    var i, n, k, obj = {};
    var int2float = function (n) { return n / 255.0; };

    // Ramp image
    obj.rampDataURI_4x16 = 'data:image/png;base64,iVBORw0KGgoAAAANS' +
            'UhEUgAAAAQAAAAQCAYAAAAxtt7zAAAAHUlEQVR4nGNkYGRiZkEGAgI' +
            'CAiNGgPHDx0+fkQUA4fgKhwHHAykAAAAASUVORK5CYII=';

    // Half ramp from 64 to 191
    obj.i_ramp_half = new Uint8Array(128);
    obj.f_ramp_half = new Float32Array(128);
    for (i = 0, n = 64; i < 128; i += 1, n += 1) {
        obj.i_ramp_half[i] = n;
        obj.i_ramp_half[i] = int2float(n);
    }

    // Full ramp from 0 to 255
    obj.i_ramp_full = new Uint8Array(256);
    obj.f_ramp_full = new Float32Array(256);
    for (i = 0; i < 256; i += 1) {
        obj.i_ramp_full[i] = i;
        obj.f_ramp_full[i] = int2float(i);
    }

    // Double ramp from -128 to 383, and clamped version
    obj.f_ramp_double = new Float32Array(512);
    obj.f_ramp_dblclip = new Float32Array(512);
    obj.i_ramp_dblclip = new Uint8Array(512);
    for (i = 0, n = -128; i < 512; i += 1, n += 1) {
        k = Math.min(Math.max(n, 0), 255);  // clamped to 0..255
        obj.f_ramp_double[i] = int2float(n);
        obj.f_ramp_dblclip[i] = int2float(k);
        obj.i_ramp_dblclip[i] = k;
    }

    return obj;
}());

buster.testCase('glim.Image', {
    'external': {
        'assertion: array_almost_equals': {
            'throws if different types': function () {
                var array = [0, 1, 2, 3];
                var array_int = new Uint8Array(array);
                buster.refute(array_almost_equals(array, array_int));
                buster.refute(array_almost_equals(array_int, new Int8Array(array)));
            },
            'throws if different length': function () {
                buster.refute(array_almost_equals([0, 1, 2, 3], [0, 1, 2]));
            },
            'works if arrays close enough': function () {
                var a = [0, 1, 2, 3];
                var b = [0, 1, 2.01, 3];
                buster.refute(array_almost_equals(a, b));
                buster.refute(array_almost_equals(a, b, 1e-4));
                buster.assert(array_almost_equals(a, b, 1e-1));
            },
            'works with empty arrays': function () {
                buster.assert(array_almost_equals([], []));
            }
        }
    },

    'constructor': {
        'setUp': function () {
            this.fromArrayStub = this.stub(glim.Image.prototype, 'fromArray');
            this.fromElementStub = this.stub(glim.Image.prototype, 'fromElement');

        },
        'creates an empty texture': function () {
            var im = new glim.Image();
            buster.assert.match(im, {'width': 0, 'height': 0});
            buster.refute.called(this.fromArrayStub);
            buster.refute.called(this.fromElementStub);
        },
        'accepts width and height arguments': function () {
            var a = 4, b = 8, c = {};
            var im = new glim.Image(a, b, c);
            buster.assert.calledOnceWith(this.fromArrayStub, a, b, c);
            im.clear();
        },
        'accepts image arguments': function () {
            var a = new Image(), b = {};
            var im = new glim.Image(a, b);
            buster.assert.calledOnceWith(this.fromElementStub, a, b);
            im.clear();
        }
    },

    '.count()': {
        'increases/decreases when created/deleted': function () {
            var count = glim.Image.count();
            var im = new glim.Image(1, 1);
            buster.assert.same(glim.Image.count(), count + 1);
            var other = new glim.Image(2, 4);
            buster.assert.same(glim.Image.count(), count + 2);
            im.clear();
            buster.assert.same(glim.Image.count(), count + 1);
            other.clear();
            buster.assert.same(glim.Image.count(), count);
        },
        'supports multiple "clear" and reallocations': function () {
            var im = new glim.Image(1, 1);
            var count = glim.Image.count();
            im.clear();
            buster.assert.same(glim.Image.count(), count - 1);
            im.clear();
            buster.assert.same(glim.Image.count(), count - 1);
            im.resize(1, 2);
            buster.assert.same(glim.Image.count(), count);
            im.getTexture();
            im.resize(4, 2);
            buster.assert.same(glim.Image.count(), count);
        },
    },

    '.toString()': {
        'works with empty image': function () {
            var im = new glim.Image();
            var str = im.toString();
            buster.assert.match(str, /\bimage\b/i);
            buster.assert.match(str, /\bempty\b/i);
            buster.refute.match(str, /\bfloat\b/i);
            buster.refute.match(str, /\bint\b/i);
        },
        'works with float image': function () {
            var im = new glim.Image(12, 34, false);
            var str = im.toString();
            buster.assert.match(str, /\bimage\b/i);
            buster.refute.match(str, /\bempty\b/i);
            buster.assert.match(str, /\bfloat\b/i);
            buster.refute.match(str, /\bint\b/i);
            buster.assert.match(str, /\b12\s*[x*]\s*34\b/i);
        },
        'works with int image': function () {
            var im = new glim.Image(42, 1, true);
            var str = im.toString();
            buster.assert.match(str, /\bimage\b/i);
            buster.refute.match(str, /\bempty\b/i);
            buster.refute.match(str, /\bfloat\b/i);
            buster.assert.match(str, /\bint\b/i);
            buster.assert.match(str, /\b42\s*[x*]\s*1\b/i);
        }
    },

    '.clear()': {
        'image remains valid': function () {
            var im = new glim.Image(1, 1);
            im.clear();
            buster.assert.match(im, {'width': 0, 'height': 0, '_texture': null});
        },
        'calls gl.deleteTexture() once': function () {
            var im = new glim.Image(1, 1);
            var texture = im.getTexture();
            var spy = this.spy(glim.getContext(), 'deleteTexture');
            im.clear();
            im.clear();
            buster.assert.calledOnce(spy);
            buster.assert.calledWithExactly(spy, texture);
        }
    },

    '.isInt(), .isFloat()': {
        'setUp': function () {
            this.im = new glim.Image();
        },
        'works when no data': function () {
            buster.assert.isFalse(this.im.isInt());
            buster.assert.isFalse(this.im.isFloat());
        },
        'works with default (float)': function () {
            this.im.resize(1, 1);
            buster.assert.isFalse(this.im.isInt());
            buster.assert.isTrue(this.im.isFloat());
            this.im.clear();
            buster.assert.isFalse(this.im.isInt());
            buster.assert.isFalse(this.im.isFloat());
        },
        'works with float': function () {
            this.im.resize(1, 1, false);
            buster.assert.isFalse(this.im.isInt());
            buster.assert.isTrue(this.im.isFloat());
        },
        'works with int': function () {
            this.im.resize(1, 1, true);
            buster.assert.isTrue(this.im.isInt());
            buster.assert.isFalse(this.im.isFloat());
        }
    },

    '.resize()': {
        'changes image size': function () {
            var im = new glim.Image();
            buster.assert.match(im, {'width': 0, 'height': 0});
            im.resize(1, 42);
            buster.assert.match(im, {'width': 1, 'height': 42});
            im.resize(7, 5, true);
            buster.assert.match(im, {'width': 7, 'height': 5});
            im.resize(8, 8, false);
            buster.assert.match(im, {'width': 8, 'height': 8});
        },
        'allocate a black image of right type': function () {
            var array = [0, 0, 0, 0, 0, 0, 0, 0];
            var im = new glim.Image();
            im.resize(1, 2);
            buster.assert.isObject(im.toArray(), new Float32Array(array));
            im.resize(1, 2, false);
            buster.assert.isObject(im.toArray(), new Float32Array(array));
            im.resize(1, 2, true);
            buster.assert.isObject(im.toArray(), new Uint8Array(array));
        },
        'throws if wrong arguments': function () {
            var im = new glim.Image();
            buster.assert.exception(function () { im.resize(1, -1); });
            buster.assert.exception(function () { im.resize(-1, 1); });
            buster.assert.exception(function () { im.resize(1, 0.5); });
            buster.assert.exception(function () { im.resize(0.5, 1); });
            buster.assert.exception(function () { im.resize(1, '1'); });
            buster.assert.exception(function () { im.resize('1', 1); });
        }
    },

    '.fromArray()': {
        'changes image size': function () {
            var im = new glim.Image();
            im.fromArray(4, 16, testData.i_ramp_full);
            buster.assert.match(im, {'width': 4, 'height': 16});
        },
        'throws if invalid length': function () {
            var im = new glim.Image();
            buster.assert.exception(function () {
                im.fromArray(4, 17, testData.i_ramp_full);
            });
            buster.assert.exception(function () {
                im.fromArray(4, 15, testData.i_ramp_full);
            });
            buster.assert.exception(function () {
                im.fromArray(5, 16, testData.i_ramp_full);
            });
            buster.assert.exception(function () {
                im.fromArray(3, 16, testData.i_ramp_full);
            });
        },
        'throws if invalid array': function () {
            var im = new glim.Image();
            buster.assert.exception(function () {
                im.fromArray(4, 16, 'toto');
            });
            buster.assert.exception(function () {
                im.fromArray(4, 16, new Int8Array(testData.i_ramp_full));
            });
            buster.assert.exception(function () {
                im.fromArray(4, 16, new Float64Array(testData.i_ramp_full));
            });
        }
    },

    '.toArray()': {
        'works in-place': {
            'with floats': function () {
                var im = new glim.Image();
                var array = new Float32Array(256);
                im.fromArray(4, 16, testData.f_ramp_full);
                var out = im.toArray(array);
                buster.assert.same(out, array);
                buster.assert(array_almost_equals(array, testData.f_ramp_full));
            },
            'with integers': function () {
                var im = new glim.Image();
                var array = new Uint8Array(256);
                im.fromArray(4, 16, testData.i_ramp_full);
                var out = im.toArray(array);
                buster.assert.same(out, array);
                buster.assert.equals(array, testData.i_ramp_full);
            },
        },
        'throws if array of wrong types/size': {
            'with floats': function () {
                var im = new glim.Image();
                im.fromArray(4, 16, testData.f_ramp_full);
                buster.assert.exception(function () {
                    im.toArray(new Float32Array(255));
                });
                buster.assert.exception(function () {
                    im.toArray(new Float64Array(256));
                });
                buster.assert.exception(function () {
                    im.toArray(new Uint8Array(256));
                });
            },
            'with integers': function () {
                var im = new glim.Image();
                im.fromArray(4, 16, testData.i_ramp_full);
                buster.assert.exception(function () {
                    im.toArray(new Uint8Array(255));
                });
                buster.assert.exception(function () {
                    im.toArray(new Int8Array(256));
                });
                buster.assert.exception(function () {
                    im.toArray(new Float32Array(256));
                });
            }
        }
    },

    '.fromArray() and .toArray()': {
        'setUp': function () {
            this.im = new glim.Image();
        },
        'works with integers (full range)': function () {
            this.im.fromArray(4, 16, testData.i_ramp_full);
            buster.assert.equals(this.im.toArray(), testData.i_ramp_full);
        },
        'works with integers (half range)': function () {
            this.im.fromArray(4, 8, testData.i_ramp_half);
            buster.assert.equals(this.im.toArray(), testData.i_ramp_half);
        },
        'works with floats (full range)': function () {
            this.im.fromArray(4, 16, testData.f_ramp_full);
            buster.assert(array_almost_equals(this.im.toArray(), testData.f_ramp_full));
        },
        'works with floats (half range)': function () {
            this.im.fromArray(4, 8, testData.f_ramp_half);
            buster.assert(array_almost_equals(this.im.toArray(), testData.f_ramp_half));
        },
        'works with floats (double range)': function () {
            this.im.fromArray(8, 16, testData.f_ramp_double);
            buster.assert(array_almost_equals(this.im.toArray(), testData.f_ramp_double));
        }
    },

    '.fromElement()': {
        'works with float': function (done) {
            var im_html = new Image();
            im_html.onload = done(function () {
                var im = new glim.Image();
                im.fromElement(im_html, false);
                buster.assert.match(im, {'width': 4, 'height': 16});
                buster.assert(array_almost_equals(im.toArray(), testData.f_ramp_full));
            });
            im_html.src = testData.rampDataURI_4x16;
        },
        'works with int': function (done) {
            var im_html = new Image();
            im_html.onload = done(function () {
                var im = new glim.Image();
                im.fromElement(im_html, true);
                buster.assert.match(im, {'width': 4, 'height': 16});
                buster.assert.equals(im.toArray(), testData.i_ramp_full);
            });
            im_html.src = testData.rampDataURI_4x16;
        },
        'works with images': function (done) {
            var im_html = new Image();
            im_html.onload = done(function () {
                var im = new glim.Image();
                im.fromElement(im_html);
                buster.assert.match(im, {'width': 4, 'height': 16});
                buster.assert(array_almost_equals(im.toArray(), testData.f_ramp_full));
            });
            im_html.src = testData.rampDataURI_4x16;
        },
        'works with canvas': function () {
            var ref = new glim.Image();
            ref.fromArray(4, 8, testData.i_ramp_half);
            var canvas = ref.toCanvas();
            var im = new glim.Image();
            im.fromElement(canvas, true);
            buster.assert.match(im, {'width': 4, 'height': 8});
            buster.assert(array_almost_equals(im.toArray(), testData.i_ramp_half));
        },
        'throws if not an element': function () {
            var im = new glim.Image();
            buster.assert.exception(function () {
                im.fromElement([0, 1, 2, 3]);
            });
            buster.assert.exception(function () {
                im.fromElement(window.document.createElement('p'));
            });
        }
    },

    '.toCanvas()': {
        'works without canvas': function () {
            var im = new glim.Image();
            im.fromArray(4, 8, testData.i_ramp_half);
            var canvas = im.toCanvas();
            buster.assert.match(canvas, {'width': 4, 'height': 8});
            var read = new glim.Image();
            read.fromElement(canvas, true);
            buster.assert.equals(read.toArray(), testData.i_ramp_half);
        },
        'works on existing canvas': function () {
            var im = new glim.Image();
            im.fromArray(4, 8, testData.i_ramp_half);
            var canvas = window.document.createElement('canvas');
            var out = im.toCanvas(canvas);
            buster.assert.same(out, canvas);
            buster.assert.match(canvas, {'width': 4, 'height': 8});
            var read = new glim.Image();
            read.fromElement(canvas, true);
            buster.assert.equals(read.toArray(), testData.i_ramp_half);
        },
        'throws if not a canvas': function () {
            var im = new glim.Image();
            buster.assert.exception(function () {
                im.toCanvas(false);
            });
            buster.assert.exception(function () {
                im.toCanvas(new Image());
            });
        },
        'throw if not an integer': function () {
            var im = new glim.Image();
            buster.assert.exception(function () {
                glim.toCanvas();
            });
            im.fromArray(4, 8, testData.f_ramp_half);
            buster.assert.exception(function () {
                glim.toCanvas();
            });
        }
    }
});