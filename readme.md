Web**GL Im**age Processing
==========================

This JavaScript library provides a high-level interface to WebGL for image processing.
Through this library, you can easily:

* create, load, and display images.
* define and run GPU functions on images through glim.Function class.

(more coming soon...)


Getting Started
---------------

Wondering where to start?

1. Have a try at the examples---especially the "getting started" one.
2. Check out the glim.Function documentation.


Example
-------

See the example---run it with python -m SimpleHTTPServer


Documentation
-------------

The documentation is set up and configured for [jsduck](https://github.com/senchalabs/jsduck).
To generate the documentation using jsduck, run from the project's root directory;

```bash
jsduck
```


Unit Testing
------------

The project is unit-tested using [Buster.JS](http://docs.busterjs.org/en/latest/).
To generate a static test page using buster.js, run from the project's root directory:

```bash
buster-static test/html/
```