/*jslint nomen: true, vars: true, plusplus: true, regexp: true */
/*global glim, check, HTMLElement, HTMLCanvasElement,
         Float32Array, Int32Array,
         WebGLShader, WebGLFramebuffer */

/** @class glim.Program
 *  This class is a higher level interface to WebGL programs.
 *
 *  You may want to use the higher-level class glim.Function instead.
 *
 *  The main components of a glim.Program are the following.
 *
 *  - The fragment and vertex shader code defined through the constructor.
 *    The default vertex shader map a `position` variable with xy values in range 0..1.
 *  - Shader #variables (uniform and attribute) which can be #set in a simple way.
 *  - The program can be #run on images.
 *    The output is either the default canvas or images (of type glim.Image).
 *
 * Example:
 *
 *     var prog = new glim.Program();         // create the identity program
 *     var img = ...;                         // get some HTML image
 *     var outcanvas = prog.run([img]);       // run the program on it
 *     console.log(prog.fragmentShaderCode);  // display the shader code
 */

// TODO: unit tests
// TODO: more doc
// TODO: what if prog.run([im], im) is called?

/** @constructor
 *  Create a new program.
 *
 *  See also: #set, #run.
 * @param {String} [fragmentShaderCode=null]
 *  Source code of the fragment shader.
 *  Default is the identity.
 * @param {String} [vertexShaderCode=null]
 *  Source code of the vertex shader.
 *  Default defines varying 2D `position` values in range 0..1.
 * @param {String} [variables={}]
 *  Shader variables initialization. Same as calling #set after creating the program.
 */
glim.Program = function (fragmentShaderCode, vertexShaderCode, variables) {
    'use strict';
    if (check) {
        check.assert.maybe.string(fragmentShaderCode);
        check.assert.maybe.string(vertexShaderCode);
        check.assert.maybe.object(variables);
    }

    // Store shader code
    if (fragmentShaderCode) {
        /** Source code of the fragment shader. @readonly @type {String} */
        this.fragmentShaderCode = fragmentShaderCode;
    }
    if (vertexShaderCode) {
        /** Source code of the vertex shader. @readonly @type {String} */
        this.vertexShaderCode = vertexShaderCode;
    }

    // Create the program
    var vShader = this._compileShader(this.vertexShaderCode, true);
    var fShader = this._compileShader(this.fragmentShaderCode, false);
    var program = this._createProgram(vShader, fShader);

    /** @private @type {Number} */
    this._verticesCount = 0;

    /** @private @type {WebGLProgram} */
    this._glProgram = program;

    /** Wrappers for shader variables. See: #set. @readonly @type {Object} */
    this.variables = {};
    this._getAttributes(this.variables);
    this._getUniforms(this.variables);

    // Set the variables
    if (!vertexShaderCode) {
        this.set({
            'aVertexPosition': glim.Program.defaultVertexPositions,
            'aTexturePosition': glim.Program.defaultTexturePositions
        }, true);
    }
    this.set(variables || {});
};

/** Set program's variables (either uniform or attribute variables).
 *
 *  Example:
 *
 *     var program = ...;  // create some program
 *     program.set({
 *         'alpha': 0.5,   // an uniform variable
 *         'position': [1, 1, 0, 1, 1, 0, 0, 0]  // an attribute
 *     });
 *
 * @param {Object} variables
 *  Variables and values, as object key/values.
 * @param {Boolean} [noError=False]
 *  If `true`, invalid variables are silently ignored.
 * @throws {ReferenceError}
 *  If a variable does not exists or is inactive (unless `noError` is set to `true`).
 */
glim.Program.prototype.set = function (variables, noError) {
    'use strict';
    if (check) {
        check.assert.object(variables);
        check.assert.maybe.boolean(noError);
    }
    var name, gl = glim.getContext();
    gl.useProgram(this._glProgram);
    for (name in variables) {
        if (variables.hasOwnProperty(name)) {
            if (this.variables[name]) {
                this.variables[name].set(variables[name]);
            } else if (!noError) {
                throw new ReferenceError('undefined or inactive shader variable: ' + name);
            }
        }
    }
};

/** Compute the output image size from the input images. @protected
 *
 *  This function is meant to be overridden: you probably don't need to call it.
 *
 *  This function is called by #run to check input dimensions and deduce output dimensions.
 * @param {Array} images
 *  Array of glim.Image or compatible HTMLElement (same as #run argument).
 * @return {Object | null}
 *  Object with numeric fields `width` and `height`.
 *  Is `null` if `images` is empty.
 * @throws {Error}
 *  If images have incompatible dimensions.
 */
glim.Program.prototype.computeOutputSize = function (images) {
    'use strict';
    var nImages = (this.variables.images && this.variables.images.size) || 0;
    if (check) {
        check.assert.integer(nImages);
        check.assert.hasLength(images, nImages);
    }

    // If no images expected, nothing to compute
    if (!nImages) {
        return null;
    }

    // Check all image's size
    var width = images[0].width, height = images[0].height;
    var k;
    for (k = 0; k < nImages; k += 1) {
        if (images[k].width !== width || images[k].height !== height) {
            throw new Error('Input images have incompatible dimensions');
        }
    }
    return {'width': width, 'height': height};
};

/** Execute the program.
 *
 *  You may want to use the higher-level #run method.
 *
 * @param {Object} variables
 *  Shader variables to be set. Same as calling #set before #exec.
 * @param {Object} opts
 *  Arguments, which must include:
 *
 *  - `width` and `height` of the output.
 *
 *  They may also include:
 *
 *  - `output` framebuffer, bound to a texture of appropriate dimensions.
 *  - `antialiasing` factor, only if rendering to canvas (i.e. no `output` provided)
 * @return {WebGLFramebuffer | HTMLCanvasElement}
 *  The output framebuffer, if specified, otherwise the rendering canvas.
 */
glim.Program.prototype.exec = function (variables, opts) {
    'use strict';
    if (check) {
        check.assert.object(opts);
        check.assert.integer(opts.width);
        check.assert.integer(opts.height);
        check.assert.positive(opts.width);
        check.assert.positive(opts.height);
        check.assert.maybe.instance(opts.output, WebGLFramebuffer);
        check.assert.maybe.positive(opts.antialiasing);
        check.assert(!opts.output || !opts.antialiasing, 'Antialiasing only apply to canvas output');
    }

    // Initialize parameters
    this.set(variables);
    var aa = opts.antialiasing || 1;
    var framebuffer = opts.output || null;
    var output = opts.output || glim.getCanvas();

    // Initialize the rendering
    var gl = glim.getContext();
    gl.useProgram(this._glProgram);
    gl.disable(gl.DEPTH_TEST);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // Set dimensions
    var renderWidth = Math.round(aa * opts.width);
    var renderHeight = Math.round(aa * opts.height);
    if (output instanceof HTMLCanvasElement) {
        output.width = renderWidth;
        output.height = renderHeight;
        output.style.width = opts.width + 'px';
        output.style.height = opts.height + 'px';
    }

    // Render
    gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer || null);
    gl.viewport(0, 0, renderWidth, renderHeight);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, this._verticesCount);

    // Clean up
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    return output;
};

/** Run the program on image(s).
 * @param {Array} images
 *  Array of input images.
 *  Accepted types are: glim.Image, or `HTMLElement` accepted by glim.Image.fromElement.
 * @param {glim.Image | Boolean} [output=null]
 *  Output image, which cannot be an input image.
 *  If `null`, output to the default rendering canvas.<br/>
 *  If boolean, `true` yields integer type and `false` yields float (similar to `useInt` argument of glim.Image.resize).
 * @param {Object} [variables={}]
 *  Shader variables to be set. This simply calls #set beforehand.
 * @param {Object} [opts={}]
 *  May contains:
 *
 *  - `width` and `height` to force output dimensions (required if `images` is empty).
 *  - `antialiasing` factor, only when rendering to canvas (i.e. when `output` is `null`).
 * @return {glim.Image | HTMLCanvasElement}
 *  The output image, or the canvas if `output` was set to `"CANVAS"`
 */
glim.Program.prototype.run = function (images, output, variables, opts) {
    'use strict';
    if (check) {
        check.assert.arrayLike(images);
        check.assert.not.includes(images, output, 'Input image cannot be used as output');
        check.assert.maybe.object(opts);
        check.assert.maybe.object(variables);
        if (check.not.boolean(output)) {
            check.assert.maybe.instance(glim.output, 'Invalid type, expected a boolean or an image');
        }
        if (opts) {
            check.assert.maybe.integer(opts.width);
            check.assert.maybe.integer(opts.height);
            check.assert.maybe.positive(opts.width);
            check.assert.maybe.positive(opts.height);
        }
    }

    // Initialize
    var gl = glim.getContext();
    gl.useProgram(this._glProgram);
    opts = opts || {};

    // Get width / height
    var outsize = this.computeOutputSize(images);
    if (outsize) {
        opts.width = opts.width || outsize.width;
        opts.height = opts.height || outsize.height;
    }
    if (!opts.width || !opts.height) {
        throw new Error('Missing output width or height');
    }
    if (this.variables.px) {
        this.variables.px.set([1 / opts.width, 1 / opts.height]);
    }

    // Bind images to textures
    var nImages = images.length;
    var imagesToClear = [];
    if (nImages) {
        var textures = (function () {
            var k, im, tex = [];
            for (k = 0; k < nImages; k += 1) {
                im = images[k];
                if (im instanceof HTMLElement) {
                    im = new glim.Image(im);
                    imagesToClear.push(im);
                }
                if (im instanceof glim.Image) {
                    tex.push(im.getTexture());
                } else {
                    throw new Error('Unsupported input image type');
                }
            }
            return tex;
        }());
        this.variables.images.set(textures);
    }

    // Initialize output to glim.Image (or null if CANVAS)
    if (typeof output === 'boolean') {
        output = new glim.Image(opts.width, opts.height, output);
    } else if (output instanceof glim.Image) {
        output.resize(opts.width, opts.height, output.isInt());
    } else if (output) {
        throw new TypeError('Invalid type for "output"');
    }
    opts.output = output ? output.bindToFramebuffer() : null;

    // Render, cleanup and return
    this.exec(variables || {}, opts);
    while (imagesToClear.length > 0) {
        imagesToClear.pop().clear();
    }
    return output || glim.getCanvas();
};

/** Remove the comments from a GLSL source code.
 *
 * @param {String} srcCode
 *  The shader source code.
 * @return {String}
 *  The same source code, with comments removed.
 */
glim.Program.stripComments = function (srcCode) {
    'use strict';
    if (check) {
        check.assert.string(srcCode);
    }
    srcCode = srcCode.replace(/\/\*.*?\*\//g, '');    // Remove /*...*/
    srcCode = srcCode.replace(/\/\/.*?[\n\r]/g, '');  // Remove //
    return srcCode;
};


// PRIVATE MEMBERS

/** Compile a WebGL shader. @private
 *
 *  See also: #_createProgram.
 * @param {String} sourceCode
 *  Source code of the shader (in GLSL).
 * @param {Boolean} [isVertexShader=False]
 *  Set to `true` if compiling the vertex shader.
 * @throws {Error}
 *  If compilation fails.
 * @return {WebGLShader}
 */
glim.Program.prototype._compileShader = function (sourceCode, isVertexShader) {
    'use strict';
    if (check) {
        check.assert.string(sourceCode);
        check.assert.maybe.boolean(isVertexShader);
    }
    var gl = glim.getContext();
    var shaderType = isVertexShader ? gl.VERTEX_SHADER : gl.FRAGMENT_SHADER;
    var shader = gl.createShader(shaderType);
    gl.shaderSource(shader, sourceCode);
    gl.compileShader(shader);
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        var error = new Error('Compilation Error.\n' + gl.getShaderInfoLog(shader));
        error.sourceCode = sourceCode;
        throw error;
    }
    return shader;
};

/** Create a program from (compiled) shaders. @private
 *
 *  See also: #_compileShader.
 * @param {WebGLShader} vertexShader
 * @param {WebGLShader} fragmentShader
 * @return {WebGLProgram}
 * @throws {Error} If link failed.
 */
glim.Program.prototype._createProgram = function (vertexShader, fragmentShader) {
    'use strict';
    if (check) {
        check.assert.instance(vertexShader, WebGLShader);
        check.assert.instance(fragmentShader, WebGLShader);
    }
    var gl = glim.getContext();
    var program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        throw new Error('Link Error: could not link shaders.');
    }
    return program;
};

/** Create wrapper (including setters) for each attribute of the current program. @private
 *
 *  See also: #_getUniforms, #variables, #set.
 * @param {Object} [object]
 *  Object into which the wrappers are appended.
 * @return {Object}
 *  Object whose properties are the GLSL attribute names, and with fields:
 *
 *  - `attribute`: has constant value `true`.
 *  - `set`: function to set this attribute's value (used in #set).
 */
glim.Program.prototype._getAttributes = function (object) {
    'use strict';
    if (check) {
        check.assert.maybe.object(object);
    }

    var gl = glim.getContext();
    var program = this._glProgram;
    var TYPE_SIZE = (function () {
        var obj = {};
        obj[gl.FLOAT] = 1;
        obj[gl.FLOAT_VEC2] = 2;
        obj[gl.FLOAT_VEC3] = 3;
        obj[gl.FLOAT_VEC4] = 4;
        return obj;
    }());

    // Create a wrapper for a single attribute
    var that = this;
    function createWrapper(attribute) {
        var loc = gl.getAttribLocation(program, attribute.name);
        var buffer = gl.createBuffer();
        var size = TYPE_SIZE[attribute.type];
        if (!size) {
            throw new TypeError('Non-float types not supported: in' + attribute.name);
        }
        var setter = function (array) {
            that._verticesCount = Math.round(array.length / size);
            gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(array), gl.STATIC_DRAW);
            gl.vertexAttribPointer(loc, size, gl.FLOAT, false, 0, 0);
        };
        gl.enableVertexAttribArray(loc);
        return {'attribute': true, 'set': setter};
    }

    // Wrap each attribute
    var wrappers = object || {};
    var attribute;
    var k, n = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
    for (k = 0; k < n; k += 1) {
        attribute = gl.getActiveAttrib(program, k);
        if (attribute) {
            wrappers[attribute.name] = createWrapper(attribute);
        }
    }
    return wrappers;
};

/** Create wrapper (including setters) for each uniform of the current program. @private
 *
 *  Thanks by [`webgl-utils.js`](https://github.com/greggman/webgl-fundamentals/blob/master/webgl/resources/webgl-utils.js)
 *  which was written by *Gregg Tavares* for [WebGLFundamentals.org](http://webglfundamentals.org).
 *
 *  See also: #_getAttribute, #variables, #set.
 * @param {Object} [object]
 *  Object into which the wrappers are appended.
 * @return {Object}
 *  Object whose properties are the GLSL uniform names, and with fields:
 *
 *  - `uniform`: has constant value `true`.
 *  - `set`: function to set this attribute's value (used in #set).
 *  - `size`, if an array: length of the uniform array.
 */
glim.Program.prototype._getUniforms = function (object) {
    'use strict';
    if (check) {
        check.assert.maybe.object(object);
    }

    var gl = glim.getContext();
    var program = this._glProgram;

    // Textures management
    var textureUnits = 1;
    var bindPoints = {};
    bindPoints[gl.SAMPLER_2D] = gl.TEXTURE_2D;
    bindPoints[gl.SAMPLER_CUBE] = gl.TEXTURE_CUBE_MAP;

    // Function to create a wrapper for a given uniform
    function createWrapper(uniform) {
        var type = uniform.type;
        var isArray = (uniform.name.substr(-3) === '[0]');
        var location = gl.getUniformLocation(program, uniform.name);
        var setter = null;
        if (type === gl.FLOAT && isArray) {
            setter = function (v) { gl.uniform1fv(location, new Float32Array(v)); };
        } else if (type === gl.FLOAT) {
            setter = function (v) { gl.uniform1f(location, v); };
        } else if (type === gl.FLOAT_VEC2) {
            setter = function (v) { gl.uniform2fv(location, new Float32Array(v)); };
        } else if (type === gl.FLOAT_VEC3) {
            setter = function (v) { gl.uniform3fv(location, new Float32Array(v)); };
        } else if (type === gl.FLOAT_VEC4) {
            setter = function (v) { gl.uniform4fv(location, new Float32Array(v)); };
        } else if ((type === gl.INT || type === gl.BOOL) && isArray) {
            setter = function (v) { gl.uniform1iv(location, new Int32Array(v)); };
        } else if (type === gl.INT || type === gl.BOOL) {
            setter = function (v) { gl.uniform1i(location, v); };
        } else if (type === gl.INT_VEC2 || type === gl.BOOL_VEC2) {
            setter = function (v) { gl.uniform2iv(location, new Int32Array(v)); };
        } else if (type === gl.INT_VEC3 || type === gl.BOOL_VEC3) {
            setter = function (v) { gl.uniform3iv(location, new Int32Array(v)); };
        } else if (type === gl.INT_VEC4 || type === gl.BOOL_VEC4) {
            setter = function (v) { gl.uniform4iv(location, new Int32Array(v)); };
        } else if (type === gl.FLOAT_MAT2) {
            setter = function (v) { gl.uniformMatrix2fv(location, false, v); };
        } else if (type === gl.FLOAT_MAT3) {
            setter = function (v) { gl.uniformMatrix3fv(location, false, v); };
        } else if (type === gl.FLOAT_MAT4) {
            setter = function (v) { gl.uniformMatrix4fv(location, false, v); };
        } else if ((type === gl.SAMPLER_2D || type === gl.SAMPLER_CUBE) && isArray) {
            var getTextureUnits = function () {
                var array = [];
                while (array.length < uniform.size) {
                    array.push(textureUnits++);
                }
                return new Int32Array(array);
            };
            var makeTexturesSetter = function (units) {
                return function (tex) {
                    gl.uniform1iv(location, units);
                    var i, t;
                    for (i = 0; i < units.length; i += 1) {
                        t = tex[i].getTexture ? tex[i].getTexture() : tex[i];
                        gl.activeTexture(gl.TEXTURE0 + units[i]);
                        gl.bindTexture(bindPoints[type], t);
                    }
                    gl.activeTexture(gl.TEXTURE0);
                };
            };
            setter = makeTexturesSetter(getTextureUnits());
        } else if (type === gl.SAMPLER_2D || type === gl.SAMPLER_CUBE) {
            var makeTextureSetter = function (unit) {
                return function (tex) {
                    var t = tex.getTexture ? tex.getTexture() : tex;
                    gl.uniform1i(location, unit);
                    gl.activeTexture(gl.TEXTURE0 + unit);
                    gl.bindTexture(bindPoints[type], t);
                    gl.activeTexture(gl.TEXTURE0);
                };
            };
            setter = makeTextureSetter(textureUnits++);
        } else {
            throw new Error('Unknown parameter type ' + uniform.name);
        }
        var wrapper = {'uniform': true, 'set': setter};
        if (isArray) {
            wrapper.size = uniform.size;
        }
        return wrapper;
    }

    // Create all the wrappers
    var wrappers = object || {};
    var uniform, name;
    var k, n = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);
    gl.useProgram(program);
    for (k = 0; k < n; k += 1) {
        uniform = gl.getActiveUniform(program, k);
        if (uniform) {
            name = uniform.name;
            if (name.substr(-3) === '[0]') {
                name = name.substr(0, name.length - 3);
            }
            wrappers[name] = createWrapper(uniform);
        }
    }
    return wrappers;
};


// MEMBER ATTRIBUTES

/** Default positions of image corners. @readonly @static @type {Array} */
glim.Program.defaultVertexPositions = [0, 0, 0, 1, 1, 0, 1, 1];

/** Default texture coordinates at corners. @readonly @static @type {Array} */
glim.Program.defaultTexturePositions = [0, 0, 0, 1, 1, 0, 1, 1];

glim.Program.prototype.vertexShaderCode = (function () {
    'use strict';
    var str = '';
    str += 'attribute vec2 aVertexPosition;                                 \n';
    str += 'attribute vec2 aTexturePosition;                              \n\n';
    str += 'varying vec2 position;                                        \n\n';
    str += 'void main(void) {                                               \n';
    str += '    position = aTexturePosition;                                \n';
    str += '    vec2 finalPosition = 2.0 * aVertexPosition - 1.0;           \n';
    str += '    gl_Position = vec4(finalPosition, 0.0, 1.0);                \n';
    str += '}                                                               \n';
    return str;
}());

glim.Program.prototype.fragmentShaderCode = (function () {
    'use strict';
    var str = '';
    str += 'precision mediump float;                                        \n\n';
    str += 'varying vec2 position;        // Current pixel position, in 0..1\n\n';
    str += 'uniform vec2 px;              // Pixel size (in 0..1)             \n';
    str += 'uniform sampler2D images[1];  // Input images                   \n\n';
    str += 'void main(void) {                                                 \n';
    str += '    vec4 color = texture2D(images[0], position);  // Pixel color  \n';
    str += '    gl_FragColor = vec4(color.rgb, color.a);      // Output color \n';
    str += '}                                                                 \n';
    return str;
}());

