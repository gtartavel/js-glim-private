/*jslint nomen: true, vars: true, browser: true */
/*global glim, check, HTMLElement, HTMLCanvasElement, WebGLFramebuffer
         Float32Array, Float64Array, ArrayBuffer, Uint8ClampedArray,
         Int8Array, Uint8Array, Int16Array, Uint16Array, Int32Array, Uint32Array
 */

/** @class glim.Image
 * A GLIM image, which is stored on the GPU.
 *
 *  This class is used in other GLIM functions (to come...)
 *
 *  Images can either have `int` or `float` type.
 */
/** @constructor
 *  Create a new GLIM image.
 *
 *  The constructor can be called either:
 *
 *  - without parameters: creates an empty image.
 *  - with same arguments as #resize.
 *  - with same arguments as #fromArray.
 *  - with same arguments as #fromElement.
 *
 *  Example:
 *
 *     var empty = new glim.Image();             // empty image
 *     var black = new glim.Image(42, 8, true);  // black image of int
 *     var canvas = ...;                         // get some HTML canvas
 *     var drawing = new glim.Image(canvas);     // image from canvas
 *
 *  See also: #clear
 *
 * @param {number | HTMLElement} [first]
 *  See: #resize, #fromArray, #fromElement
 * @param {number | Boolean} [second]
 *  See: #resize, #fromArray, #fromElement
 * @param {boolean | Float32Array | Uint8Array} [third]
 *  See: #resize, #fromArray.
 */
glim.Image = function (first, second, third) {
    'use strict';
    glim.getContext();  // Throws if no context is available

    /** Image width. @readonly @type {Number} */
    this.width = 0;
    /** Image height. @readonly @type {Number} */
    this.height = 0;
    /** Whether to use uint8 or float32. @private @type {Boolean | null} */
    this._useInt = null;

    // Initialize
    if (typeof first === 'number' && typeof second === 'number') {
        this.fromArray(first, second, third);
    } else if (first instanceof HTMLElement) {
        if (check) {
            check.assert.not.assigned(third);
        }
        this.fromElement(first, second);
    } else if (check) {
        check.assert.not.assigned(first);
        check.assert.not.assigned(second);
        check.assert.not.assigned(third);
    }
};

/** Get the number of non-empty images. @static
 *
 *  If too high, consider #clear-ing your images!
 *
 *  This may be useful for debug.
 * @return {Number}
 */
glim.Image.count = function () {
    'use strict';
    return glim.Image._count;
};

/** Conversion to string.
 * @return {String}
 */
glim.Image.prototype.toString = function () {
    'use strict';
    var prefix = this.width + 'x' + this.height;
    var suffix = '';
    if (this.isFloat()) {
        suffix = ' of float';
    } else if (this.isInt()) {
        suffix = ' of int';
    } else {
        prefix = 'empty';
    }
    return '[' + prefix + ' glim.Image' + suffix + ']';
};

/** Check if the image contains integer data (in range 0..255).
 * @return {Boolean}
 */
glim.Image.prototype.isInt = function () {
    'use strict';
    return (this._useInt === true);
};

/** Check if the image contains floating-point data.
 * @return {Boolean}
 */
glim.Image.prototype.isFloat = function () {
    'use strict';
    return (this._useInt === false);
};

/** Clear the image (and release its GPU memory).
 *
 *  This is especially useful when processing streams (video or webcam).
 *
 *  Clearing several time an image is safe.
 *  Cleared image can be reused (e.g. using #resize, #fromArray, #fromElement).
 *
 *  Example:
 *
 *     im = new glim.Image(4, 8);  // create an image
 *     ...
 *     im.clear();  // release the memory
 *     im.clear();  // useless, but safe
 *     im.fromElement(canvas);  // re-allocate memory
 *
 *  See also: #count
 */
glim.Image.prototype.clear = function () {
    'use strict';
    this._useInt = null;
    this.width = this.height = 0;
    if (this._texture) {
        var gl = glim.getContext();
        gl.deleteTexture(this._texture);
        this._texture = null;
        glim.Image._count -= 1;
    }
};

/** Resize the image (it is initialized with zeros).
 *
 *  Example:
 *
 *     var im = new glim.Image(16, 16):  // constructor version
 *     im.resize(24, 32);        // resize to a 24x32 image
 *     im.resize(24, 32, true);  // force int instead of float
 *
 *  See also: #fromArray, #fromElement
 *
 * @param {Number} width
 * @param {Number} height
 * @param {Boolean} [useInt=False]
 *  If `true`, uses integers (in 0..255) rather than floating-point values.
 */
glim.Image.prototype.resize = function (width, height, useInt) {
    'use strict';
    if (check) {
        check.assert.maybe.boolean(useInt);  // width and height checked in .fromArray()
    }
    this.fromArray(width, height, useInt);
};

/** Load the image from a typed array.
 *
 *  The array must contain the RGBA values of each pixels, row by row.
 *
 *  A #Float32Array results in an image of type `float`, a #Uint8Array of type `int`.
 *
 *  Example:
 *
 *     var array = ...;  // some array
 *     var im = glim.Image(32, 24, new Float32Array(array));  // constructor version
 *     im.fromArray(32, 24, new Uint8Array(array));           // int image
 *     im.fromArray(1, 1, new Float32Array([0, 0.5, 1, 1]));  // mono-pixel
 *
 *  See also: #resize, #fromElement, #toArray
 *
 * @param {Number} width
 * @param {Number} height
 * @param {Uint8Array | Float32Array | Boolean} [data = False]
 *  If a boolean, same as #resize `useInt` argument.
 *  If a typed array, must be of size `4*width*height`.
 * @throws {Error}
 *  If the input array has wrong type or wrong size.
 */
glim.Image.prototype.fromArray = function (width, height, data) {
    'use strict';
    if (check) {
        check.assert.integer(width);
        check.assert.integer(height);
        check.assert.positive(width);
        check.assert.positive(height);
    }
    var gl = glim.getContext();

    // Handle input type
    var useInt = false;
    if (data === undefined || data === null || data === false) {
        data = null;
    } else if (data === true) {
        data = null;
        useInt = true;
    } else if (data instanceof Uint8Array) {
        useInt = true;
    } else if (!(data instanceof Float32Array)) {
        throw new Error('Invalid data type');
    }

    // Check size
    if (data && data.length !== 4 * width * height) {
        throw new Error('Invalid array size');
    }

    // Set texture
    var type = useInt ? gl.UNSIGNED_BYTE : gl.FLOAT;
    gl.bindTexture(gl.TEXTURE_2D, this.getTexture());
    try {
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, type, data);
    } finally {
        gl.bindTexture(gl.TEXTURE_2D, null);
    }
    this._useInt = useInt;
    this.width = width;
    this.height = height;
};

/** Load the image from an HTML element (image, canvas, or video).
 *
 *  If you load images from a video stream, consider #clear-ing or re-using image:
 *  creating many glim.Image without #clear-ing them may drastically slow down your app.
 *
 *  Example:
 *
 *     var canvas = ..., video = ...;    // get some HTML elements
 *     var im = new glim.Image(canvas);  // constructor version
 *     im.fromElement(video, true);      // force int type
 *
 *  See also: #fromArray, #toCanvas
 *
 * @param {HTMLElement} image
 *  Image to be loaded. Can be either: `img`, `canvas`, or `video` element.
 * @param {Boolean} [useInt=False]
 *  If `true`, uses integers (in 0..255) rather than floating-point values.
 */
glim.Image.prototype.fromElement = function (image, useInt) {
    'use strict';
    if (check) {
        check.assert.instance(image, HTMLElement);
        check.assert.maybe.boolean(useInt);
    }
    var gl = glim.getContext();
    var type = useInt ? gl.UNSIGNED_BYTE : gl.FLOAT;
    gl.bindTexture(gl.TEXTURE_2D, this.getTexture());
    try {
        gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
        // gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);  // flip the image
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, type, image);
    } finally {
        gl.bindTexture(gl.TEXTURE_2D, null);
    }
    this._useInt = Boolean(useInt);
    this.width = image.width;
    this.height = image.height;
};

/** Export the image to an array.
 *
 *  Integer images are stored as #Uint8Array, float as #Float32Array.
 *
 *  Example:
 *
 *     var canvas = ...;                 // get some HTML canvas
 *     var im = new glim.Image(canvas);  // read some image (float)
 *     var array = im.toArray();         // returns a Float32Array
 *     var out = new Float32Array(4*im.width*im.height);
 *     im.toArray(out);                  // export to the given array
 *
 *  See also: #toCanvas, #fromArray
 *
 * @param {Float32Array | Uint8Array} [outArray]
 *  Array to be filled. Must be big enough and match the image type.
 * @return {Float32Array | Uint8Array}
 *  Array containing the RGBA values of pixels, row by row.<br/>
 *  Its length is `4*width*height` and type is `Float32Array` if #isFloat or `Uint8Array` if #isInt.
 * @throws {Error}
 *  If the output array does not match image type, or if not big enough.
 */
glim.Image.prototype.toArray = function (array) {
    'use strict';
    var size = 4 * this.width * this.height;

    // Create/check output array type
    if (this._useInt === true) {
        if (!array) {
            array = new Uint8Array(size);
        } else if (!(array instanceof Uint8Array)) {
            throw new Error('Expected Uint8Array to export image of type int');
        }
    } else if (this._useInt === false) {
        if (!array) {
            array = new Float32Array(size);
        } else if (!(array instanceof Float32Array)) {
            throw new Error('Expected Float32Array to export image of type float');
        }
    } else {
        throw new Error('Image is empty!');
    }

    // Check size
    if (array.length < size) {
        throw new Error('Output array is not big enough');
    }

    // Fill and return array
    var gl = glim.getContext();
    var type = this._useInt ? gl.UNSIGNED_BYTE : gl.FLOAT;
    try {
        this.bindToFramebuffer();
        gl.readPixels(0, 0, this.width, this.height, gl.RGBA, type, array);
    } finally {
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }
    return array;
};

/** Export an integer image to a canvas.
 *
 *  Example:
 *
 *     var video = ..., canvas = ...;   // get some HTML elements
 *     var im = new glim.Image(video);  // create an image
 *     var c = im.toCanvas();           // export it to a new canvas
 *     im.toCanvas(canvas);             // export it to an existing canvas
 *
 *  See also: #toArray, #fromElement
 *
 * @param {HTMLCanvasElement} [outCanvas]
 *  The canvas to draw the image in.
 *  If no canvas is given, a new canvas is created.
 * @return {HTMLCanvasElement}
 *  The resulting canvas.
 * @throws {Error}
 *  If the image is not integer, or the canvas cannot be written to (e.g. if already used in another context).
 */
glim.Image.prototype.toCanvas = function (canvas) {
    'use strict';
    if (check) {
        check.assert.maybe.instance(canvas, HTMLCanvasElement);
    }

    if (!this.isInt()) {
        throw new Error('Can only export integer images to canvas');
    }

    canvas = canvas || window.document.createElement('canvas');
    canvas.width = this.width;
    canvas.height = this.height;

    var ctx2d = canvas.getContext('2d');
    if (!ctx2d) {
        throw new Error('Cannot get canvas\'s 2D context');
    }

    var imdata = ctx2d.createImageData(this.width, this.height);
    imdata.data.set(new Uint8ClampedArray(this.toArray()));
    ctx2d.putImageData(imdata, 0, 0);
    return canvas;
};

/** Get the WebGL texture, and create it if needed.
 *
 *  You probably don't need this method.
 *  However, it may be useful for WebGL interoperability.
 * @param {Boolean} [throwIfNone=False]
 *  If `true`, raise an `Error` if the texture does not exists.
 * @return {WebGLTexture}
 */
glim.Image.prototype.getTexture = function (throwIfNone) {
    'use strict';
    if (check) {
        check.assert.maybe.boolean(throwIfNone);
    }
    if (!this._texture) {
        if (throwIfNone) {
            throw new Error('Image is empty');
        }
        var gl = glim.getContext();
        this.clear();  // reset width/height/type
        this._texture = gl.createTexture();
        if (!this._texture) {
            throw new Error('Something went wrong with gl.createTexture()');
        }
        glim.Image._count += 1;
        try {
            gl.bindTexture(gl.TEXTURE_2D, this._texture);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        } finally {
            gl.bindTexture(gl.TEXTURE_2D, null);
        }
    }
    return this._texture;
};

/** Bind a framebuffer as current and attach the texture to it.
 *
 *  You probably don't need this method.
 *  However, it may be useful for WebGL interoperability.
 * @param {WebGLFramebuffer} [framebuffer]
 *  If you want to use a specific framebuffer.
 * @return {WebGLFramebuffer}
 */
glim.Image.prototype.bindToFramebuffer = function (framebuffer) {
    'use strict';
    if (check) {
        check.assert.maybe.instance(framebuffer, WebGLFramebuffer);
    }
    var gl = glim.getContext();
    framebuffer = framebuffer || glim.Image._getFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.getTexture(), 0);
    return framebuffer;
};


// PRIVATE MEMBERS

/** Number of allocated images. See #count. @private @type {Number} */
glim.Image._count = 0;

/** Get a framebuffer object. @private @static
 * @return {WebGLFramebuffer}
 */
glim.Image._getFramebuffer = function () {
    'use strict';
    if (!glim.Image._framebuffer) {
        var gl = glim.getContext();
        glim.Image._framebuffer = gl.createFramebuffer();
    }
    return glim.Image._framebuffer;
};
