/*jslint vars: true, nomen: true, browser: true */
/*global glim: true, check: true,
         Event, FileReader, FileList, File, HTMLCanvasElement */

/** Namespace for "check-types" library, if available.
 *
 * This library is used for argument's types checking at runtime.
 *
 * Please refer to the [library's documentation](https://www.npmjs.com/package/check-types).
 *
 * @class @singleton
 */
check = (function () {
    'use strict';
    if (window.check === undefined) {
        console.info('check-types library not detected: type checking disabled');
        return null;
    }
    return check;
}());

/** Namespace for GLIM.
 *
 * @class @singleton
 */
glim = {};

/** Check whether WebGL is available and supports floating-points.
 *
 * If not available, most of the library's functions won't run.
 *
 * @return {Boolean}
 *  `true` iff WebGL is enabled and supported by the browser.
 */
glim.isAvailable = function () {
    'use strict';
    return Boolean(glim.getContext(true));
};

/** Get GLIM's HTML canvas.
 *
 * The returned canvas provides the WebGL context.
 * It is also used as default output.
 *
 * @return {HTMLCanvasElement}
 */
glim.getCanvas = function () {
    'use strict';
    if (!glim._canvas) {
        var canvas = window.document.createElement('canvas');
        canvas.style.transform = 'scale(1, -1)';
        glim._canvas = canvas;
    }
    return glim._canvas;
};

/** Get GLIM's WebGL context.
 *
 * This is the WebGL context used by GLIM.
 *
 * See also: #isAvailable, #getCanvas
 *
 * @param {Boolean} [nothrow=False]
 * @throws {Error}
 *  if WebGL is not available and if `nothrow` argument is not true.
 * @return {WebGLRenderingContext | null}
 */
glim.getContext = function (nothrow) {
    'use strict';
    if (check) {
        check.assert.maybe.boolean(nothrow);
    }
    if (glim._context === undefined) {
        var canvas = glim.getCanvas();
        var gl = null;

        // Get WebGL context
        try {
            gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');
        } catch (e) {
            gl = null;
        }
        if (!gl && !nothrow) {
            throw new Error('WebGL is disabled or not supported by your browser');
        }

        // Get float extension
        if (gl && !gl.getExtension('OES_texture_float')) {
            gl = null;
            if (!nothrow) {
                throw new Error('WebGL texture-float extension not supported');
            }
        }

        glim._context = gl;
    }
    return glim._context;
};

/** Execute the callback next time the frame is redrawn.
 *
 *  Example:
 *
 *     var render = function () {
 *         ...  // some rendering code
 *         glim.requestAnimationFrame(render);  // render again after a while
 *     };
 *     render();  // start the loop
 *
 * @param {Function} callback
 *  The function to be called at next frame.
 */
glim.requestAnimationFrame = (function () {
    'use strict';
    var fcn = window.requestAnimationFrame       ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame    ||
              function (fcn) { window.setTimeout(fcn, 1000 / 60); };
    return fcn.bind(window);
}());

/** Read a file.
 *
 *  Example:
 *
 *     var file = ...;  // get some file, e.g. from form input
 *     glim.readFile(file, 'text', function (txt) {
 *         alert('Content of ' + this.name + '\n\n' + txt);
 *     });
 *
 *  See also: glim.getFileLoader
 * @param {File} file
 *  File to be loaded.
 * @param {String} format
 *  Format in which the file should be read: 'text', 'dataURL', or 'ArrayBuffer'.
 * @param {Function} callback
 *  Once the file is loaded, this function is called with file content as argument and `this=file`.
 * @param {Function} [onerror]
 *  Called in case of error, with the `Error` object as argument and `this=file`.
 * @throws {Error}
 *  In case `onerror` argument is not specified, the error is thrown.
 */
glim.readFile = function (file, format, callback, onerror) {
    'use strict';
    if (check) {
        check.assert.instance(file, File);
        check.assert.string(format);
        check.assert.match(format, /text|dataurl|arraybuffer/i);
        check.assert.function(callback);
        check.assert.maybe.function(onerror);
    }
    onerror = onerror || function (e) {
        throw e;
    };
    var readAsFormat = {
        'text': 'readAsText',
        'dataurl': 'readAsDataURL',
        'arraybuffer': 'readAsArrayBuffer'
    }[format.toLowerCase()];
    if (!readAsFormat) {
        throw new Error('Unknown requested format: ' + format);
    }
    var reader = new FileReader();
    reader.onload = function () {
        callback.call(file, reader.result);
    };
    reader.onerror = function () {
        onerror.call(file, new Error('Cannot read file: ' + file.name));
    };
    reader.onabort = function () {
        onerror.call(file, new Error('Abort while reading file: ' + file.name));
    };
    reader[readAsFormat](file);
};

/** Get a function for file loading.
 *
 *  The returned function can be used as a callback for click or dragover/drop listeners.
 *
 *  Example:
 *
 *      var callbacks = {'*:text': function (txt) { alert(txt); }};
 *      var loader = glim.getFileLoader(false, callbacks);
 *      var elmt = document.getElementById('div_drop');
 *      elmt.onclick = loader;     // prompt for files when clicked
 *      elmt.ondrop = loader;      // load files when dropped
 *      elmt.ondragover = loader;  // prevent default behavior during drag-over
 *
 * @param {Boolean} multiple
 *  Whether multiple files can be handled at the same time.
 * @param {Object} callback
 *  Provide a callback function for each MIME type.
 *
 *  Format is: `"type1[, type2[, ...]][:format]" => function (data) { ... }`
 *
 *  Example*:
 *
 *      var callbacks = {
 *          'video/​*':  function (dataurl) { ... },  // loading function
 *          'images/jpeg,images/png': 'video/​*',     // use the same function
 *          '*:text': function (text) { ... }        // default: read text data
 *      }
 *
 *  (*) In the last example, an invisible character was insterted between the `/` and `*` characters.
 *      When copy-pasted this example, it may cause an error if not removed.
 *
 * @param {Function} [onerror]
 *  Error handling function, called with the `Error` object as argument
 *  and the loaded `file` argument as `this`.
 *
 * @return {Function}
 *  A loading function, taking as parameter either:
 *
 *  - nothing: prompts for file(s) and load them.
 *  - a `File` or an array of `File`: load the file(s).
 *  - an `Event`:
 *      - drop event: load the dropped file.
 *      - click even: prompts for file(s) and load them.
 *      - another event: ignore it (and prevent default event behavior).
 *
 * @throws {Error}
 *  If `callbacks` argument has invalid syntax.
 */
glim.getFileLoader = function (multiple, callbacks, onerror) {
    'use strict';
    if (check) {
        check.assert.boolean(multiple);
        check.assert.object(callbacks);
        check.assert.maybe.function(onerror);
    }
    onerror = onerror || function (e) {
        throw e;
    };

    // Pre-process callback
    var accepted = [];      // array of accepted MIME types
    var mime_handler = {};  // MIME => {'callback': X, 'format': X}
    (function () {
        var key, i, fcn, parts, mimes, format;
        for (key in callbacks) {
            if (callbacks.hasOwnProperty(key)) {

                // Allow intra-callbacks reference
                fcn = callbacks[key];
                if (typeof fcn === 'string') {
                    fcn = callbacks[fcn];
                }
                if (typeof fcn !== 'function') {
                    throw new Error('Invalid callback for type: ' + key);
                }

                // Create an entry for each type
                parts = key.trim().split(/\s*:\s*/, 2);
                mimes = parts[0].split(/\s*,\s*/);
                format = parts[1];
                for (i = 0; i < mimes.length; i += 1) {
                    accepted.push(mimes[i]);
                    mime_handler[mimes[i]] = {
                        'callback': fcn,
                        'format': format || 'dataurl'
                    };
                }
            }
        }
    }());

    // Get MIME type handler (with 'callback' and 'format' fields)
    var getMIMEHandler = function (mime) {
        var subtypes, str, handler;
        for (subtypes = mime.split(/\s*\/\s*/); subtypes.length > 0; subtypes.pop()) {
            str = subtypes.join('/');
            handler = mime_handler[str] || mime_handler[str + '/*'];
            if (handler) {
                return handler;
            }
        }
        return mime_handler['*'] || mime_handler[''] || null;
    };

    // Create a file picker
    var file_picker = (function () {
        var elmt = window.document.createElement('input');
        elmt.setAttribute('type', 'file');
        elmt.accept = accepted.join(',');
        elmt.multiple = Boolean(multiple);
        return elmt;
    }());

    // File loading function
    var loader = function (file) {
        var i, evt, handler;
        if (file === undefined || file === null) {
            file_picker.click();
        } else if (file instanceof Event) {
            evt = file;
            evt.preventDefault();
            if (evt.type === 'drop') {
                loader(evt.dataTransfer.files);
            } else if (evt.type === 'click') {
                loader();
            }
        } else if (file && file.length && file[0] instanceof File) {
            if (!multiple && file.length > 1) {
                onerror.call(file, new Error('Cannot handle multiple files'));
            } else {
                for (i = 0; i < file.length; i += 1) {
                    loader(file[i]);
                }
            }
        } else if (file instanceof File) {
            handler = getMIMEHandler(file.type);
            if (!handler) {
                onerror.call(file, new Error('MIME type not accepted: ' + file.type));
            } else {
                glim.readFile(file, handler.format, handler.callback);
            }
        } else {
            onerror.call(file, new Error('Invalid "file" argument'));
        }
    };

    // We're almost done...
    file_picker.onchange = function () {
        if (this.files) {
            loader(this.files);
        }
    };
    return loader;
};

/* TODO
function downloadURI(name, uri)
{
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
}
 */