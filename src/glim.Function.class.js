/*jslint vars: true, nomen: true */
/*global glim, check */

/** @class glim.Function
 *
 *  Define a GLSL function which can be #run on images.
 *  The function is applied pixel-wise on a set of input images.
 *  It takes as arguments the values of a given pixel in each of the input images.
 *  It returns the new RGB or RGBA value of this pixel.
 *
 *  More details about how to write functions are given in the #constructor.
 *
 *  Example:
 *
 *     var fcn = new glim.Function();  // create the identity function
 *     console.log(fcn.sourceCode);    // display the function source code...
 *     console.log(fcn.program.fragmentShaderCode);  // ... and the generated shader
 *     var img = ...;                  // get some HTML image
 *     var canvas = fcn.run(img);      // run the function, output to canvas
 *
 *  The default identity function is equivalent to:
 *
 *     var sourceCode = 'vec4 function (vec4 color) { return color; }'
 *     var fcn = new glim.Function(sourceCode);
 *
 *  When #run, the function generate an image which may either be drawn into
 *  the default glim canvas or stored into an existing or new glim.Image:
 *
 *     var fcn = new glim.Function();      // create a function
 *     var img = ...;                      // get some HTML image
 *     var imOut = fcn.run([img], False);  // render to a new glim.Image of float
 *     var array = imOut.toArray();        // get the image content
 *     fcn.run([imOut]);                   // run on the output, render to the canvas
 *
 *  The function may take any number of arguments:
 *
 *     var srcCode = 'vec3 function (vec3 x, vec3 y) { return x + y; }'
 *     var add = new glim.Function(srcCode);   // create the function
 *     var img = ...;                          // get some HTML image
 *     var imOut = add.run([img, img], True);  // run and returns a glim.Image of int
 *
 *  As a particular case, if the function takes no argument,
 *  the output size must be explicitly provided to the #run function:
 *
 *     var srcCode = 'vec3 function () { return vec3(position, 0.0); }';
 *     var makeColorGradient = new glim.Function(srcCode);
 *     makeColorGradient.run([], null, {'width': 320, 'height': 240});
 *
 *  Note: the `position` variable used above provides the normalized xy coordinates
 *  of the current pixel; more details in the #constructor documentation.
 *
 *  The function can depend on some parameters: they are called _uniform variables_.
 *  They can be #set at any time and also directly when the function is #run:
 *
 *     var amplify = new Function([
 *         'uniform float factor;',         // amplification factor
 *         'vec3 function (vec3 color) {',
 *         '    return factor * color;'     // multiply value by this factor
 *         '}'
 *     ].join('\n'));
 *     var img = ...;               // get some HTML image
 *     amplify.set({'factor': 2});  // set 'factor' to 2
 *     amplify.run([img]);          // run the program
 *     amplify.run([img], null, {}, {'factor': 3});  // set facotr=3 and run
 *
 *  More details and examples are provided in the #constructor documentation.
 */

/** @constructor
 *  Define a GLSL function, which can later be run on images.
 *
 *  The GLSL function takes as arguments the value of a pixel in one or several images.
 *  It computes and returns the new value for this pixel.
 *
 *  The GLSL function must meet the following requirements:
 *
 *  - it must be called `function`.
 *  - it must return either a `vec3` (RGB) or `vec4` (RGBA) vector.
 *  - its arguments must all have the type returned by the function.
 *  - it may have any number of arguments.
 *
 *  Moreover, the function can make use of the following global uniform variables:
 *
 *  - `vec2 position`: the position of the current pixel, in range 0..1.
 *  - `vec2 px`: the spacing between two neighboring pixels.
 *  - `sampler2D images[]`: an array whose size is the number of arguments of `function`.
 *
 *  Moreover, you may use the GLSL standard functions.
 *  In particular, `texture2D` may be useful.
 *  You can have a look at the [GLSL quick reference](http://mew.cx/glsl_quickref.pdf)
 *      to have an overview of the language.
 *
 *  Example:
 *
 *     // Invert the color of an image
 *     var invert = glim.Function(
 *         'vec3 function (vec3 color) {' +
 *         '    return 1.0 - colors;  // inverse the color' +
 *         '}'
 *     );
 *
 *  Example:
 *
 *     // Convert an image to black with alpha channel
 *     var grayToAlpha = glim.Function(
 *         'vec4 function (vec4 color) {' +
 *         '    float gray = dot(color.rgb, vec3(1.0/3.0);  // pixel gray value' +
 *         '    return vec4(vec3(0.0), 1.0 - gray);         // map gray to alpha' +
 *         '}'
 *     );
 *
 *  Example:
 *
 *     // Linear combination of two images
 *     var linearMix = glim.Function(
 *         'uniform float alpha;          // a global parameter' +
 *         'vec3 function (vec3 a, vec3 b) {' +
 *         '    return mix(a, b, alpha);  // linear combination' +
 *         '}'
 *     );
 *
 *  Example:
 *
 *     // Horizontal gradient
 *     var gradx = glim.Function(
 *         'vec3 function (vec3 color) {' +
 *         '    left = texture2D(images[0], position - px * vec2(1.0, 0.0);' +
 *         '    return color - left.rgb;'
 *         '}'
 *     );
 *
 * @param {String} [sourceStringGLSL]
 *  The GLSL function, as a string.
 *  Default is the identity function.
 * @param {Object} [variables={}]
 *  Initial values for the uniform variables (see #set).
 */
glim.Function = function (sourceStringGLSL, variables) {
    'use strict';
    if (check) {
        check.assert.maybe.string(sourceStringGLSL);
        check.assert.maybe.object(variables);
    }

    var k;
    sourceStringGLSL = sourceStringGLSL || [
        'vec4 function (vec4 color) {',
        '   return color;',
        '}'
    ].join('\n');

    // Get arguments number and type
    var srcCode = glim.Program.stripComments(sourceStringGLSL);
    var proto = srcCode.match(/\b(vec[34])\s+function\s*\(([\w\s,]*)\)(?=\s*\{)/);
    if (!proto) {
        throw new Error('Cannot find GLSL function: has it right name and return type?');
    }
    var functionPrototype = proto[0] + ';\n\n';
    var argType = proto[1];
    var args = (proto[2].trim() === '') ? [] : proto[2].split(/\s*,\s*/);
    var argCount = args.length;
    var regex;
    for (k = 0; k < argCount; k += 1) {
        regex = new RegExp('\\b' + argType + '\\b');
        if (!regex.test(args[k])) {
            throw new TypeError('Does not support mixed arguments types in GLSL function');
        }
    }

    // Create parts of the function
    var ext = {'vec3': '.rgb', 'vec4': ''}[argType];
    var fragColor = {'vec4': 'color', 'vec3': 'vec4(color.rgb, 1.0)'}[argType];
    var argList = [], indent = '        ';
    for (k = 0; k < argCount; k += 1) {
        argList.push(indent + 'texture2D(images[' + k + '], position)' + ext);
    }

    // Create the function
    var str = '';
    str += 'precision mediump float;                                        \n\n';
    str += 'varying vec2 position;        // Current pixel position, in 0..1\n\n';
    str += 'uniform vec2 px;              // Pixel size (in 0..1)             \n';
    if (argCount) {
        str += 'uniform sampler2D images[' + argCount + '];  // Input images\n\n';
    }
    str += '// User function prototype                                        \n';
    str += functionPrototype;
    str += 'void main(void) {                                                 \n';
    str += '    ' + argType + ' color = function(                             \n';
    str += argList.join(',\n') + '\n    );                                    \n';
    str += '    gl_FragColor = ' + fragColor + ';                             \n';
    str += '}                                                               \n\n';
    str += '// User function                                                  \n';
    str += sourceStringGLSL + '                                               \n';

    /** The underlying program. @readonly @type {glim.Program} */
    this.program = new glim.Program(str, null, variables);
    /** The source code of the function @type {String} */
    this.sourceCode = sourceStringGLSL;
};

/** Set the values of uniform variables.
 *
 *  Note that unused variables are likely to be optimized away: setting them would throw an exception.
 *
 *  Example:
 *
 *     var linearMix = glim.Function(...);  // as defined in constructor's examples
 *     linearMix.set({'alpha': 0.5});
 *
 * @param {Object} variables
 *  Object whose properties are the variables names and with desired values.
 */
glim.Function.prototype.set = function (variables) {
    'use strict';
    this.program.set(variables);
};

/** Run the function.
 *
 * @param {Array} images
 *  Array of images, which can be a glim.Image or a HTMLElement
 *  supported by glim.Image.fromElement (`img`, `video`, `canvas`).
 * @param {glim.Image | Boolean} [output=null]
 *  - If `null`, output is the default canvas, glim.getCanvas.
 *  - If a glim.Image, render to this image (while preserving its type).
 *  - If a boolean, create a new image of float (if `false`) or int (if `true`).
 * @param {Object} [variables={}]
 *  Uniform variables to be #set.
 *  It is similar to calling #set before rendering.
 * @param {Object} [opts={}]
 *  Specific rendering options.
 *  In particular, `width` and `height` controls the output dimensions (default: same as input images).
 *  In case of a no-argument function, `width` and `height` must be given (since there is no input image).
 * @returns {HTMLCanvasElement | glim.Image}
 *  The output image (if `output` is specified and not `null`) or the rendering canvas.
 */
glim.Function.prototype.run = function (images, output, variables, opts) {
    'use strict';
    return this.program.run(images, output, variables, opts);
};
